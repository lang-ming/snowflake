## 适用于laravel octane的雪花算法扩展包
### 基于swoole扩展, 利用swoole table和进程锁实现相同时间内的序列号自增
### 环境需求
>* php >=7.3
>* laravel >= 6
>* swoole >= 4.6
### 生成规则（默认）
<table>
    <tr>
        <th>符号位</th>
        <th>时间戳位</th>
        <th>序列号位</th>
        <th>业务id位</th>
        <th>机器id位</th>
    </tr>
    <tr>
        <th>0</th>
        <th>秒级（最大37位）</th>
        <th>16位，最大65535</th>
        <th>5位，最大31</th>
        <th>5位，最大31</th>
    </tr>
</table>

### 设计思路
* 秒级时间戳设计， 原版雪花算法为41位毫秒级时间戳，在集群环境下很容易出现毫秒级的误差，由于时间戳在高位，进而丧失趋势递增，对插入有一定的影响。
* 默认位数可在在配置文件中修改，一旦确定不能修改。 例如我将时间戳定为30位，其他配置修改后总和永远为64位。 2的30次幂/3600/24/365大约可用34年。
* 服务器在时间同步下，容易出现时间回退的情况（时间回拨）,这里采用发现当前时间小于上一次生成时间，则以上一次生成时间为基础，序列号相应递增。
* 如果发现序列号满了，则借用下一秒的时间戳。 极端情况：借用时间后刚好服务重启，swoole内存表清空，重启后的时间仍然大于上次生成时间，此时会出现id重复。
* 关于workerid，服务器id如果手动配置会在部署时非常麻烦，这里采用获取服务器mac地址，自动注册到db里并获取id，自动注册发生在boot阶段，不会对后续的请求造成性能影响。
* swoole内存表的逻辑是先读取，再做逻辑判断和操作，然后回写。为了保证原子性，应用swoole的进程锁来实现。
* 利用二进制位运行，可获得更高的生成速度。

### 安装扩展包
>1. composer引入此包 `composer require langming/snowflake -vvv`
>2. 发布配置文件 `php artisan vendor:publish` 选择tag:snowflake-config或者服务提供者(类似SnowFlake)
>3. 如果没有找到要发布的配置文件的tag或者服务提供者 执行`composer dump-autoload`
>4. 继续执行步骤2，确保配置文件发布
>5. 修改配置文件，定义各部分所占位数，定义workerid自动注册的表连接名称和表名称。
>6. 执行`php artisan migrate` 创建表

### 使用示例
```
use Langming\Snowflake\Facades\Snowflake;
use Langming\Snowflake\Snowflake as SnowflakeService;
class HomeController extends Controller
{
    public function index(Request $request)
    {
        //使用门面生成id
        $id = Snowflake::create();
        
        //解析生成的id(获得各部分的值)
        $arr = Snowflake::parse($id);
        
        //如果你不想使用已注册的单例
        //应确保先执行config方法来传入配置。
        new SnowflakeService()->config($config)->create();
    }
}
```

