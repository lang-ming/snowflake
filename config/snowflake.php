<?php
/**
 * snowflake配置
 * @author: langming
 * @date: 2021/8/26 下午2:42
 */

return [
    //基准时间
    'epoch' => env('SNOWFLAKE_EPOCH', '2021-01-01 00:00:00'),
    //序列号所占位数（16位：秒级并发数6万）
    'sequence_bits' => env('SNOWFLAKE_SEQUENCE_BITS', 16),
    //业务id所占位数（5位：31）
    'data_center_id_bits' => env('SNOWFLAKE_DATA_CENTER_ID_BITS', 5),
    //机器id所占位数（5位：31）
    'worker_id_bits' => env('SNOWFLAKE_WORKER_ID_BITS', 5),

    //业务id
    'data_center_id' => env('SNOWFLAKE_DATA_CENTER_ID', 1),
    //机器id
    'worker_id' => env('SNOWFLAKE_WORKER_ID', 0),
    //数据库连接配置 用于生成workerid
    'db_connection' => env('SNOWFLAKE_DB_CONN', 'mysql'),
    //表名称配置
    'db_table' => env('SNOWFLAKE_DB_TABLE', 'snowflake_worker'),
];
