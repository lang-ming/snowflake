<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSnowflakeWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = config('snowflake');
        if (!Schema::connection($config['db_connection'])->hasTable($config['db_table'])){
            Schema::connection($config['db_connection'])->create($config['db_table'], function (Blueprint $table) {
                $table->increments('id');
                $table->integer('datacenter_id');
                $table->text('mac');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $config = config('snowflake');
        Schema::dropIfExists($config['db_table']);
    }
}
