<?php
/**
 * snowflake 门面
 * @author: langming
 * @date: 2021/8/26 下午1:52
 */
namespace Langming\Snowflake\Facades;

use Illuminate\Support\Facades\Facade;

class Snowflake extends Facade
{
    protected static function getFacadeAccessor():string
    {
        return 'Langming\Snowflake\Snowflake';
    }
}